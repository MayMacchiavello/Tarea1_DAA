
import random
import time


def Partition(a):
  #Caso Base
  if len(a)==1:
    return([],a[0],[])
  if len(a)==2:
    if a[0]<=a[1]:
      return([],a[0],a[1])
    else:
      return([],a[1],a[0])

  p = random.randint(0,len(a)-1)  ## pivote random
  pivot = a[p]
  right = []
  left = []
  for i in range(len(a)):
    if not i == p:
      if a[i] > pivot:
        right.append(a[i])
      else:
        left.append(a[i])
  return(left, pivot, right)

def QuickSelect(a,k):

  (left,pivot,right) = Partition(a)
  if len(left)==k-1:
    result = pivot
  elif len(left)>k-1:
    result = QuickSelect(left,k)
  else:
    result = QuickSelect(right,k-len(left)-1)
  return result

a=[]
infile=open('datos7.txt','r')
for line in infile:
    b=int(line)
    a.append(b)
infile.close()

#x = input("Ingresar el termino a buscar: ")
x=random.randint(0,6400000)

tiempo_inicial=time.time()
QuickSelect(a,x)
tiempo_final=time.time()
tiempo_ejecucion=tiempo_final-tiempo_inicial
#print "Se demora %f segundos en buscar el %d-esimo termino." % (tiempo_ejecucion,x)
print tiempo_ejecucion
