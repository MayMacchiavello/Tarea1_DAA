import heapq
import random
import time

def heapSearch( bigArray, k ):
    heap = []
    # Note: below is for illustration. It can be replaced by 
    # heapq.nlargest( bigArray, k )
    for item in bigArray:
        # If we have not yet found k items, or the current item is larger than
        # the smallest item on the heap,
        if len(heap) < k or item > heap[0]:
            # If the heap is full, remove the smallest element on the heap.
            if len(heap) == k: heapq.heappop( heap )
            # add the current element as the new smallest.
            heapq.heappush( heap, item )
    return heap

arreglo = []
archivo = open("datos4.txt", "r")
for linea in archivo:
    b=int(linea)
    arreglo.append(b)
archivo.close()

#x = input("Ingresar el ranking a buscar: ")
x=random.randint(1,800000)

tiempo_inicial = time.time()
heapSearch( arreglo, x )
tiempo_final = time.time()
tiempo_ejecucion= tiempo_final-tiempo_inicial
print tiempo_ejecucion